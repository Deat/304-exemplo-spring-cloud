package br.com.mastertech.imersivo.pessoa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PessoaController {
	
	@Autowired
	private PessoaService pessoaService;
	
	@GetMapping("/pessoa/{nome}/{modelo}")
	public Pessoa criaPessoa(@PathVariable String nome, @PathVariable String modelo) {
		return pessoaService.criaPessoa(nome, modelo);
	}

}
